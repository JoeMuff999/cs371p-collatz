// --------------
// Hackerrank.c++
// --------------
// --------
// Purpose: the submission file for the hackerrank
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <sstream>  // istringstream
#include <string>   // getline, string
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

using namespace std;
const int ONE_MILLION = 1000000;

// ------------
// collatz_read
// ------------

pair<int, int> collatz_read (const string& s) {
    istringstream sin(s);
    int i, j;
    sin >> i >> j;
    return make_pair(i, j);
}

// ------------
// get_cycle_length
// ------------
int get_cycle_length(const int number, int* cache)
{
    int cycle_counter = 1;
    //need an unsigned long long due to overflow. don't need to worry about casting stuff because number always greater than 0
    unsigned long long n = number;
    while(n > 1)
    {
        if(n < ONE_MILLION && cache[n] != 0)
        {
            //justttt in case something goes horribly wrong
            assert(n >= 0 && n < ONE_MILLION);
            return cache[n] + cycle_counter - 1;
        }

        if(n%2 == 0)
            n = n/2;
        else
        {
            //optimization from class. if odd, just do both operations at once
            n = n + n/2 + 1;
            cycle_counter++;
        }
        cycle_counter++;
    }
    return cycle_counter;
}

// ------------
// collatz_eval
// ------------
/*
    Preconditions/argument validity:
    0 < i, j < 1,000,000

    Postconditions:
    none

    return-value validity:
    largest_cycle_length > 0
*/
tuple<int, int, int> collatz_eval (const pair<int, int>& p) {


    int i, j;
    tie(i, j) = p;

    //assert argument validity/ preconditions
    assert(i > 0 && j > 0 && i < ONE_MILLION && j < ONE_MILLION);

    int start = i < j ? i : j; // lowest number out of i and j is the "start"
    int end = i > j ? i : j; // highest number out of i and j is the "end"

    //real start is an optimization from class. if end = m, then the largest cycle length is found from m/2 +1 to m.
    int real_start = end/2 + 1 > start ? end/2 + 1 : start;
    int largest_cycle_length = 0;

    //initializing lazy cache
    int *lazy_cache = new int[ONE_MILLION];

    for(int count = real_start; count <= end; ++count)
    {
        int cycle_count = get_cycle_length(count, lazy_cache);
        assert(count < ONE_MILLION && count >= 0);
        lazy_cache[count] = cycle_count;
        if(cycle_count > largest_cycle_length)
        {
            largest_cycle_length = cycle_count;
        }
    }
    //return value validity
    assert(largest_cycle_length > 0);
    //deallocate the cache memory
    delete[] lazy_cache;
    return make_tuple(i, j, largest_cycle_length);
}

// -------------
// collatz_print
// -------------

void collatz_print (ostream& sout, const tuple<int, int, int>& t) {
    int i, j, v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}

// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    string s;
    while (getline(sin, s))
        collatz_print(sout, collatz_eval(collatz_read(s)));
}

// ----
// main
// ----

int main () {
    collatz_solve(cin, cout);
    return 0;
}
