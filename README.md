# CS371p: Object-Oriented Programming Collatz Repo

* Name: Joseph Muffoletto

* EID: jrm7925

* GitLab ID: joemuff999

* HackerRank ID: JoeMuff

* Git SHA: 5a718fb5a515d9bdeaa6f16be7ac145e5ae210eb

* GitLab Pipelines: https://gitlab.com/JoeMuff999/cs371p-collatz/-/pipelines

* Estimated completion time: 5

* Actual completion time: 7

* Comments: cool project! I found the issues quite helpful. Some things were not so clear, but I'm sure it'll get easier. Looking forward to doing a more intensive project!
