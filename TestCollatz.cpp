// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// ----
// read
// ----

TEST(CollatzFixture, read) {
    ASSERT_EQ(collatz_read("1 10\n"), make_pair(1, 10));
}

TEST(CollatzFixture, read1) {
    ASSERT_EQ(collatz_read("10 1\n"), make_pair(10, 1));
}

// ----
// get_cycle_length
// ----

TEST(CollatzFixture, get_cycle_length0) {
    int * test_cache = new int[1000000]();
    ASSERT_EQ(get_cycle_length(1, test_cache), 1);
    delete []test_cache;

}

TEST(CollatzFixture, get_cycle_length1) {
    int * test_cache = new int[1000000]();
    test_cache[2] = 2;
    ASSERT_EQ(get_cycle_length(999999, test_cache), 259);
    delete []test_cache;
}

TEST(CollatzFixture, get_cycle_length2) {
    int * test_cache = new int[1000000]();
    test_cache[2] = 2;
    ASSERT_EQ(get_cycle_length(871, test_cache), 179);
    delete []test_cache;
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    ASSERT_EQ(collatz_eval(make_pair(1, 10)), make_tuple(1, 10, 20));
}

TEST(CollatzFixture, eval1) {
    ASSERT_EQ(collatz_eval(make_pair(100, 200)), make_tuple(100, 200, 125));
}

TEST(CollatzFixture, eval2) {
    ASSERT_EQ(collatz_eval(make_pair(201, 210)), make_tuple(201, 210, 89));
}

TEST(CollatzFixture, eval3) {
    ASSERT_EQ(collatz_eval(make_pair(900, 1000)), make_tuple(900, 1000, 174));
}

TEST(CollatzFixture, eval4) {
    ASSERT_EQ(collatz_eval(make_pair(1, 1)), make_tuple(1, 1, 1));
}

TEST(CollatzFixture, eval5) {
    ASSERT_EQ(collatz_eval(make_pair(1, 999999)), make_tuple(1, 999999, 525));
}

TEST(CollatzFixture, eval6) {
    ASSERT_EQ(collatz_eval(make_pair(10, 1)), make_tuple(10, 1, 20));
}

TEST(CollatzFixture, eval7) {
    ASSERT_EQ(collatz_eval(make_pair(999999, 1)), make_tuple(999999,1, 525));
}


// TEST(CollatzDeathTest, eval0) {
//     int i,j;
//     i = j = 0;
//     ASSERT_DEATH(collatz_eval(make_pair(i,j)), "Assertion `i > 0 && j > 0 && i < ONE_MILLION && j < ONE_MILLION' failed.");
// }

// TEST(CollatzDeathTest, eval1) {
//     int i,j;
//     i = j = 1000000;
//     ASSERT_DEATH(collatz_eval(make_pair(i,j)), "Assertion `i > 0 && j > 0 && i < ONE_MILLION && j < ONE_MILLION' failed.");
// }


// -----
// print
// -----

TEST(CollatzFixture, print) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 10, 20));
    ASSERT_EQ(sout.str(), "1 10 20\n");
}

TEST(CollatzFixture, print1) {
    ostringstream sout;
    collatz_print(sout, make_tuple(1, 2, 3));
    ASSERT_EQ(sout.str(), "1 2 3\n");
}

TEST(CollatzFixture, print2) {
    ostringstream sout;
    collatz_print(sout, make_tuple(3, 2, 1));
    ASSERT_EQ(sout.str(), "3 2 1\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream sin("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n");
}

TEST(CollatzFixture, solve1) {
    istringstream sin("1 1");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 1 1\n");
}

TEST(CollatzFixture, solve2) {
    istringstream sin("1 999999");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "1 999999 525\n");
}

TEST(CollatzFixture, solve3) {
    istringstream sin("999999 1");
    ostringstream sout;
    collatz_solve(sin, sout);
    ASSERT_EQ(sout.str(), "999999 1 525\n");
}



